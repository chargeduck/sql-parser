# SqlParser

## 1. 介绍
解析传入的sql文件，生成单体springboot项目

## 2. 代码说明
### 2.1 配置类说明

#### 2.1.1 TemplateConfig

> 这个配置是总的配置类，用来存放用户选择的生成配置

| 序号 | 类型                   | 属性             | 描述                                                         |
| ---- | ---------------------- | ---------------- | ------------------------------------------------------------ |
| 1    | GenerateConfig         | generateConfig   | 生成配置，是否生成entity，controller等                       |
| 2    | UserChoose             | userChoose       | 用户选择，是否使用lombok 是否使用日志注解                    |
| 3    | List<TableInfo>        | tableList        | 解析sql文件生成的 表对象或者迁都单填写                       |
| 4    | List<CommonProperties> | commonProperties | 填写此值之后，会抽离出来一个BaseEntity作为所有表实体类的父类，统一进行管理 |
| 5    | String                 | database         | 数据库类型，目前仅支持mysql、oracle且oracle解析不好用        |

#### 2.1.2 GenerateConfig

> 生成类配置

| 序号 | 类型    | 属性              | 描述                                                         |
| ---- | ------- | ----------------- | ------------------------------------------------------------ |
| 1    | String  | basePackage       | 基础包名，package xxx;后边的这个                             |
| 2    | String  | author            | 作者，用来表明谁写的类                                       |
| 3    | Boolean | saveToLocal       | 是否保存到本地，没啥用，上了前端页面之后通过浏览器生成肯定是保存到本地 |
| 4    | Boolean | enableEntity      | 是否开启生成实体类                                           |
| 5    | Boolean | enableController  | 是否开启生成Controller                                       |
| 6    | Boolean | enableService     | 是否开启生成Controller                                       |
| 7    | Boolean | enableServiceImpl | 是否开启生成Controller                                       |
| 8    | Boolean | enableMapper      | 是否开启生成Controller                                       |
| 9    | Boolean | enableXml         | 是否开启生成Controller                                       |
| 10   | Boolean | enableVue         | 是否开启生成Controller                                       |
| 11   | Boolean | enableApiJs       | 是否开启生成Controller                                       |
| 12   | Boolean | enableLayui       | 是否开启生成Controller                                       |
| 13   | Boolean | enableLog         | 是否开启生成Controller                                       |



## 3. 使用说明



## 4. 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 5. 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

## 6. 存在的问题

- [x] 生成entity未导入@Data和@Api注解

- [x] mpaper接口类未引入entity类

- [x] service接口类未引入entity类

- [x] impl实现类未引入entity类及Service类，

- [x] service impl中下载模板的方法没有返回类型

- [ ] mapper.xml中未实现 save update del方法

- [x] service 的export2Excel方法后边未加 ;

- [x] controller delById 复制成了getById 

- [x] removeById返回类型为boolean不是integer

- [x] controller层下载导入模板方法调用错误

- [x] controller层新增或保存方法返回boolean不是integer

- [x] ResultFactory需要添加一个根据boolean或者integer返回的类

- [ ] serviceimpl部分接口未实现

- [ ] mapper xml 部分接口未实现

- [x] 数据库注释包含空格时注释获取不全

- [ ] pom.xml未按照需求引入依赖，现在是全导入

- [x] ~~分页未添加是否使用IPage或者PageHelper~~ IPage需要但需添加@Param 费劲

- [ ] BaseEntity公共参数未实现

- [x] BaseEntity未引入相关注解

- [x] controller下载模板方法有误

- [x] @MapperScan方法导入错误

- [x] impl引入service包错误

- [x] ResultFactory引入PageParam类错误

- [x] 



