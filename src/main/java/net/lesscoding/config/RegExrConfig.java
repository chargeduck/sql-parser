package net.lesscoding.config;

import cn.hutool.core.util.ReUtil;

/**
 * @author eleven
 * @date 2022/7/29 10:08
 * @description 正则表达式配置类
 * 具体校验规则可以去以下两个网址进行可视化查看
 * https://regexr.com/
 * https://jex.im/regulex/#
 */
public class RegExrConfig {
    /**
     * Mysql对应String类型的数据类型
     * 匹配varchar char tinytext mediumtext longtext
     */
    public static final String MYSQL_STRING = "(var)*char(\\(\\d+\\))?|(tiny|medium|long)*text";
    /**
     * Mysql对应Integer的的数据类型
     * 匹配tinyint smallint mediumint int
     */
    public static final String MYSQL_INTEGER = "(tiny|small|medium)*int(\\(\\d+\\))?";
    /**
     * Mysql对应Boolean的类型
     * 匹配tinyint(1) bit(1) boolean
     */
    public static final String MYSQL_BOOLEAN = "(tinyint|bit)\\(1\\)|boolean";
    /**
     * Mysql对应double或者BigDecimal的类型
     * 匹配decimal,double类型，按照用户选择是否使用BigDecimal来判断
     */
    public static final String MYSQL_DECIMAL = "(decimal|double)(\\(\\d+,\\d+\\))?";
    /**
     * Mysql对应的Long类型
     * 匹配bigint
     */
    public static final String MYSQL_LONG = "bigint(\\(\\d+\\))?";
    /**
     * Mysql对应的Date Timestamp 或者LocalDatetime的类型
     * 匹配date datetime timestamp
     */
    public static final String MYSQL_DATE ="(date|timestamp|datetime)(\\(\\d+\\))?";
    /**
     * Mysql对应LocalTIme类型
     * 匹配time
     */
    public static final String MYSQL_LOCAL_TIME = "time(\\(\\d+\\))?";
    /**
     * Mysql对应的key的字段
     * 匹配primary key(word) 或者foreign key(word)
     */
    public static final String MYSQL_KEY = "(primary|foreign)\\s+key\\s*(\\(\\w+\\))(\\s*\\S)*";
    /**
     * mysql主键 匹配导出ddl语句中的主键字段
     * 匹配primary key(id)
     */
    public static final String MYSQL_PRIMARY_KEY = "primary\\s+key\\s*(\\(\\w+\\))(\\s*\\S)*";
    /**
     * 匹配手从书写的主键字段
     * id int(11) default 0 auto_increment primary key comment 'test'
     */
    public static final String MYSQL_IN_COL_PRIMARY_KEY = "(\\s*\\S*)*primary\\s+key\\s*(\\s*\\S*)*";
}
