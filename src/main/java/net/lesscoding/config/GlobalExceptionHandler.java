package net.lesscoding.config;

import lombok.extern.slf4j.Slf4j;
import net.lesscoding.common.Result;
import net.lesscoding.common.ResultFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author eleven
 * @date 2022/7/29 18:42
 * @apiNote 全局异常处理器
 */
@Component
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler{
    /**
     * 处理运行时异常
     * @param   e       运行时异常对象
     * @return  Result  通用返回结果类
     */
    @ExceptionHandler({RuntimeException.class})
    public Result runtimeExceptionHandler(RuntimeException e){
        log.error("==服务异常==",e);
        return ResultFactory.exception(e);
    }
}
