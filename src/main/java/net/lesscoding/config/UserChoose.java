package net.lesscoding.config;

import lombok.Data;

/**
 * @author eleven
 * @date 2022/7/13 13:37
 * @description
 */
@Data
public class UserChoose {
    /**
     * 是否使用LocalDateTime作为时间类型
     */
    private Boolean enableLocalDateTime;
    /**
     * 是否使用Date作为时间类型
     */
    private Boolean enableDate;
    /**
     * 是否使用Timestamp作为时间类型
     */
    private Boolean enableTimestamp;
    /**
     * 是否使用BigDecimal作为double类型的javaType
     */
    private Boolean enableBigDecimal;
    /**
     * 是否使用Lombok代替get/set
     */
    private Boolean enableLombok;
    /**
     * 是否使用EasyExcel
     */
    private Boolean enableEasyExcel;
}
