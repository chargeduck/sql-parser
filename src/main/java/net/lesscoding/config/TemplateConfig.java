package net.lesscoding.config;

import lombok.Data;
import net.lesscoding.common.CommonProperties;
import net.lesscoding.common.TableInfo;

import java.util.List;

/**
 * @author eleven
 * @date 2022/7/13 17:36
 * @description 生成模板配置
 */
@Data
public class TemplateConfig {
    /**
     * 生成配置
     */
    private GenerateConfig generateConfig;
    /**
     * 用户选择生成
     */
    private UserChoose userChoose;
    /**
     * 表集合
     */
    private List<TableInfo> tableList;

    /**
     * 公共属性
     */
    private List<CommonProperties> commonProperties;
    /**
     * 数据库类型
     */
    private String databaseType;
    /**
     * 数据库名称
     */
    private String database;


}
