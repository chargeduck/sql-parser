package net.lesscoding.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author eleven
 * @date 2022/8/6 12:59
 * @description Knife配置类
 */
@Component
public class KnifeConfig {
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .description("# Class Assistant Restful APIs")
                        .termsOfServiceUrl("http://lesscoding.net")
                        //.contact("2496290990@qq.com")
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("1.0.0版本")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("net.lesscoding.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}
