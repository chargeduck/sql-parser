package net.lesscoding.config;

import lombok.Data;

/**
 * @author eleven
 * @date 2022/7/13 17:05
 * @description 生成配置
 */
@Data
public class GenerateConfig {
    /**
     * 基础包
     */
    private String basePackage;
    /**
     * 作者
     */
    private String author;
    /**
     * 是否保存到本地
     */
    @Deprecated
    private Boolean saveToLocal;
    /**
     * 开启生成实体类
     */
    private Boolean enableEntity;
    /**
     * 开始生成controller
     */
    private Boolean enableController;
    /**
     * 开启生成Service
     */
    private Boolean enableService;
    /**
     * 开启生成impl
     */
    private Boolean enableServiceImpl;
    /**
     * 开启生成Mapper
     */
    private Boolean enableMapper;
    /**
     * 开启生成xml
     */
    private Boolean enableXml;
    /**
     * 开启生成Vue文件
     */
    private Boolean enableVue;
    /**
     * 开启生成Layui文件
     */
    private Boolean enableLayui;
    /**
     * 开启自定义注解
     */
    private Boolean enableLog;

    /**
     * 开始基础配置生成
     */
    private Boolean enableBase;
}
