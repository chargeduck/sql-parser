package net.lesscoding.parse;

import cn.hutool.core.util.ReUtil;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.lesscoding.common.TableInfo;
import net.lesscoding.config.UserChoose;
import net.lesscoding.config.RegExrConfig;
import net.lesscoding.utils.ParseUtil;

import java.util.List;

/**
 * @author eleven
 * @date 2022/7/18 13:22
 * @description
 */
@NoArgsConstructor
@AllArgsConstructor
public class OracleParser extends ParseUtil {

    private UserChoose userChoose;

    /**
     * 解析单条sql
     * @param sql sql语句
     * @return
     */
    @Override
    public TableInfo parseSql(String sql) {
        return null;
    }

    /**
     * 解析sql建表语句集合
     * @param sqlList
     * @return
     */
    @Override
    public List<TableInfo> parseSql(List<String> sqlList) {
        return null;
    }

    /**
     * 直接解析数据库管理工具导出的sql文件
     * @param sql
     * @return
     */
    @Override
    public List<TableInfo> parseExportSql(String sql) {

        return null;
    }

    @Override
    public String getFieldTypeByDataType(String dataType) {
        if(ReUtil.isMatch(RegExrConfig.MYSQL_STRING,dataType)){
            return "String";
        }
        if(ReUtil.isMatch(RegExrConfig.MYSQL_BOOLEAN,dataType)){
            return "Boolean";
        }
        if(ReUtil.isMatch(RegExrConfig.MYSQL_INTEGER,dataType)){
            return "Integer";
        }
        if(ReUtil.isMatch(RegExrConfig.MYSQL_DECIMAL,dataType)){
            return userChoose.getEnableBigDecimal() ? "BigDecimal":"Double";
        }
        if(ReUtil.isMatch(RegExrConfig.MYSQL_LONG,dataType)){
            return "Long";
        }
        if(ReUtil.isMatch(RegExrConfig.MYSQL_DATE,dataType)){
            if (userChoose.getEnableDate()){
                return "Date";
            }
            if (userChoose.getEnableTimestamp()){
                return "Timestamp";
            }
            if (userChoose.getEnableLocalDateTime()){
                return "LocalDateTime";
            }
        }
        if(ReUtil.isMatch(RegExrConfig.MYSQL_LOCAL_TIME,dataType)){
            return "LocalTime";
        }
        return "String";
    }

    @Override
    public String getComment(String columnStr) {
        return null;
    }

    @Override
    public void getComment(String columnStr, String commentStr) {
        
    }
}
