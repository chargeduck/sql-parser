package net.lesscoding;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author eleven
 * @date 2022/7/11 14:55
 * @description
 */
@SpringBootApplication
@MapperScan("net.lesscoding.mapper")
@EnableKnife4j
public class SqlParseApplication {
    public static void main(String[] args) {
        SpringApplication.run(SqlParseApplication.class,args);
    }
}
