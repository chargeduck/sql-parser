package net.lesscoding.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author eleven
 * @date 2022/8/6 13:37
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageParam<T> {
    /**
     * 当前页
     */
    @ExcelIgnore
    @TableField(exist = false)
    private Integer currentPage;

    /**
     * 每页展示条数
     */
    @ExcelIgnore
    @TableField(exist = false)
    private Integer pageSize;

    /**
     * 数据量
     */
    @ExcelIgnore
    @TableField(exist = false)
    private Long total;

    /**
     * 数据
     */
    @ExcelIgnore
    @TableField(exist = false)
    private List<T> data;

    public Integer getCurrentPage() {
        return currentPage == null ? 0 : currentPage;
    }

    public Integer getPageSize() {
        return pageSize == null ? 10 : pageSize;
    }

}
