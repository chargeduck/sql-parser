package net.lesscoding.entity;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author eleven
 * @date 2022/8/6 13:37
 * @description
 */
@Data
public class BaseEntity<T> extends PageParam<T> {

    @ExcelIgnore
    @ApiModelProperty("创建人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createBy;

    @ExcelIgnore
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String createTime;

    @ExcelIgnore
    @ApiModelProperty("更新人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    @ExcelIgnore
    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateTime;

    @ExcelIgnore
    @ApiModelProperty("删除标记0未删除1已删除")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @TableLogic(value = "0",delval = "1")
    private Boolean delFlag;
}
