package net.lesscoding.service;

import net.lesscoding.common.Result;
import net.lesscoding.config.TemplateConfig;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @author eleven
 * @date 2022/7/14 11:20
 * @description
 */
public interface GenerateService {
    /**
     * 执行生成方法
     * @param file              传入过来的文件
     * @param configJson        配置json字符串
     * @param response          客户端响应
     */
    void doGenerate(MultipartFile file, String configJson, HttpServletResponse response);

    /**
     * 解析SQL获取结果
     * @param file          要解析的sql文件
     * @param configJson    选择的的配置
     * @return
     */
    Result parseSql(MultipartFile file, String configJson);

    /**
     * 从表格生成代码
     * @param config        用户选择额配置信息以及创建的表信息
     * @param response      客户端响应
     */
    void doGenerate(TemplateConfig config, HttpServletResponse response);
}
