package net.lesscoding.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.lesscoding.common.Result;
import net.lesscoding.config.TemplateConfig;
import net.lesscoding.service.GenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @author eleven
 * @date 2022/7/14 10:49
 * @description
 */
@RestController
@RequestMapping("/gen")
@Api(tags = "代码生成器")
public class GenerateController {
    @Autowired
    private GenerateService generateService;

    @PostMapping("/doGenByFile")
    @ApiOperation(value = "根据sql文件生成",notes = "导入sql文件是需要标明databaseType类型")
    public void doGenerateByFile(MultipartFile file, String configJson, HttpServletResponse response){
        generateService.doGenerate(file,configJson,response);
    }

    @PostMapping("/doGenByTable")
    @ApiOperation(value = "根据表单生成",notes = "用户自己在前端自己选择创建")
    public void doGenerateByTable(@RequestBody TemplateConfig config, HttpServletResponse response){
        generateService.doGenerate(config,response);
    }

    @PostMapping("/parse")
    @ApiOperation(value = "解析sql")
    public Result parseSql(MultipartFile file, String configJson){
        return generateService.parseSql(file,configJson);
    }
}
