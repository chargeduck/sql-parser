package net.lesscoding.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author eleven
 * @date 2022/7/29 13:09
 * @description 通用返回类
 */
@Data
@AllArgsConstructor
public class Result {
    private Integer code;

    private String msg;

    private Object data;

    private Result() {}
}
