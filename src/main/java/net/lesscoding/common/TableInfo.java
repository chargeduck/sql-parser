package net.lesscoding.common;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.List;

/**
 * @author eleven
 * @date 2022/7/13 13:55
 * @description 表
 */
@Data
public class TableInfo {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 表注释
     */
    private String tableComment;
    /**
     * 用于生成实体类名称或者是方法名称
     */
    private String className;
    /**
     * 御用生成本类对象
     */
    private String fieldName;

    private ColumnInfo keyCol;

    private List<ColumnInfo> columnList;

    public String getClassName(){
        String camelCase = getFieldName();
        return camelCase.substring(0,1).toUpperCase() + camelCase.substring(1);
    }

    public String getFieldName(){
        return StrUtil.toCamelCase(tableName);
    }
}
