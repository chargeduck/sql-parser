package net.lesscoding.common;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

/**
 * @author eleven
 * @date 2022/7/13 13:56
 * @description
 */
@Data
public class ColumnInfo {
    /**
     * 数据库字段
     */
    private String columnName;
    /**
     * 字段名称
     */
    private String fieldName;
    /**
     * 数据库类型
     */
    private String dataType;
    /**
     * 字段类型
     */
    private String fieldType;
    /**
     * 字段注释
     */
    private String comment;

    /**
     * 是否查询字段
     */
    private Boolean searchField = true;
    /**
     * 是否模糊查询
     */
    private Boolean fuzzySearch;
    /**
     * 是否是日期类型字段
     */
    private Boolean isDateCol = false;
    /**
     * 日期类型的pattern
     */
    private String pattern;

    private String methodName;

    public String getFieldName(){
        return StrUtil.toCamelCase(columnName);
    }

    public String getMethodName(){
        String camelCase = getFieldName();
        return camelCase.substring(0,1).toUpperCase() + camelCase.substring(1);
    }

    public String getPattern(){
        if(StrUtil.isNotBlank(pattern)){
            return pattern;
        }
        if(isDateCol){
            if(CommonConst.DATE_TYPE.contains(fieldType)){
                return "yyyy-MM-dd HH:mm:ss";
            }
            if(CommonConst.TIME.contains(fieldType)){
                return "HH:mm:ss";
            }
        }
        return "";
    }
    @Override
    public String toString() {
        return "ColumnInfo{" +
                "columnName='" + columnName + '\'' +
                ", fieldName='" + getFieldName() + '\'' +
                ", dataType='" + dataType + '\'' +
                ", fieldType='" + fieldType + '\'' +
                ", comment='" + comment + '\'' +
                ", searchField=" + searchField +
                ", fuzzySearch=" + fuzzySearch +
                ", isDateCol=" + isDateCol +
                ", pattern='" + pattern + '\'' +
                ", methodName='" + getMethodName() + '\'' +
                '}';
    }
}
