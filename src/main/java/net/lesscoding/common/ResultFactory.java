package net.lesscoding.common;

import com.github.pagehelper.PageInfo;
import net.lesscoding.entity.PageParam;

/**
 * @author eleven
 * @date 2022/7/29 13:47
 * @description
 */
public class ResultFactory {
    /**
     * 生成Layui成功返回队形
     * @param data  要返回的数据
     * @return
     */
    public static Result layuiSuccess(Object data){
        return new Result(CommonConst.LAYUI_SUCCESS_CODE,CommonConst.SUCCESS_MSG,data);
    }

    /**
     * 返回Layui成功结果
     * @param msg   成功信息
     * @return
     */
    public static Result LayuiSuccess(String msg){
        return new Result(CommonConst.LAYUI_SUCCESS_CODE,msg,null);
    }

    /**
     * 通用成功返回
     * @param msg   成功信息
     * @return
     */
    public static Result success(String msg){
        return new Result(CommonConst.SUCCESS_CODE,msg,null);
    }

    /**
     * 通用成功返回
     * @param data  要返回的数据信息
     * @return
     */
    public static Result success(Object data){
        return new Result(CommonConst.SUCCESS_CODE,CommonConst.SUCCESS_MSG,data);
    }

    /**
     * 通用失败返回
     * @param msg       错误信息
     * @return
     */
    public static Result failed(String msg){
        return new Result(CommonConst.FAILED_CODE,msg,null);
    }

    /**
     * 通用异常返回   用于全局异常处理器
     * @param t 异常信息
     * @return
     */
    public static Result exception(Throwable t){
        return new Result(CommonConst.FAILED_CODE,t.getMessage(),null);
    }

    /**
     * 构建通用返回
     * @param code      返回状态码
     * @param msg       返回信息
     * @param data      返回数据
     * @return
     */
    public static Result build(Integer code,String msg,Object data){
        return new Result(code,msg,data);
    }

    /**
     * 根据自己写的sql语句 更新结果返回    更新，新增，删除，大于0的时候返回成功，否则返回失败
     * @param effectRow     受影响的行
     * @return
     */
    public static Result buildByResult(Integer effectRow){
        return effectRow != null && effectRow > 0 ? success("操作成功") : failed("操作失败");
    }

    /**
     * 根据调用IService或者BaseMapper自带接口返回的结果构建Result
     * @param flag     调用结果
     * @return
     */
    public static Result buildByResult(Boolean flag){
        return flag ? success("操作成功") : failed("操作失败");
    }

    /**
     * 返回分页结果
     * @param info    PageInfo的分页返回数据
     * @return
     */
    public static Result returnPage(PageInfo info){
        PageParam param = new PageParam(info.getPageNum(),
                info.getPageSize(),info.getTotal(),info.getList());
        return success(param);
    }
}
