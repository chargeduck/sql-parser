package net.lesscoding.common;

import lombok.Data;

/**
 * @author eleven
 * @date 2022/7/13 13:54
 * @description
 */
@Data
public class CommonProperties {
    /**
     * 公共属性的类型
     */
    private String type;
    /**
     * 公共属性的字段
     */
    private String column;
}
