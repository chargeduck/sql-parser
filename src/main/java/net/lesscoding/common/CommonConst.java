package net.lesscoding.common;


/**
 * @author eleven
 * @date 2022/7/15 11:00
 * @description
 */
public class CommonConst {
    public static final String MYSQL = "mysql";
    public static final String ORACLE = "oracle";
    public static final String CREATE = "create";
    public static final String LEFT_BRACKET = "(";
    public static final String RIGHT_BRACKET = ")";
    public static final String COMMA = ",";
    public static final String SEMICOLON = ";";
    public static final String WHITE_SPACE = " ";
    public static final String COMMENT = "comment";
    public static final Integer SUCCESS_CODE = 200;
    public static final Integer LAYUI_SUCCESS_CODE = 0;
    public static final Integer FAILED_CODE = 500;
    public static final String SUCCESS_MSG = "success";
    public static final String FAILED_MSG = "failed";
    public static final String DATE_TYPE = "date,datetime,timestamp";
    public static final String TIME = "time";
}
