/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : generate_sys

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 09/08/2022 09:54:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for out_path_config
-- ----------------------------
DROP TABLE IF EXISTS `out_path_config`;
CREATE TABLE `out_path_config`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `model` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '模块',
  `second_model` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '二级模块',
  `temp_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '模板路径',
  `out_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '输出路径',
  `use_base_package` bit(1) NULL DEFAULT NULL COMMENT '是否使用基础包',
  `ui_flag` bit(1) NULL DEFAULT NULL COMMENT '是否是ui路径',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` bit(1) NULL DEFAULT NULL COMMENT '删除标记 0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '输出路径配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of out_path_config
-- ----------------------------
INSERT INTO `out_path_config` VALUES (1, 'curd', 'entity', '/templates/end/entity.java.vm', '%s/src/main/java/%s/entity/%s.java', b'1', b'0', '参数描述：数据库名或者sql文件名,基础包名，表名转换的类名,用于生成实体类。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (2, 'curd', 'controller', '/templates/end/controller.java.vm', '%s/src/main/java/%s/controller/%sController.java', b'1', b'0', '参数描述：数据库名或者sql文件名,基础包名，表名转换的类名,用于生成Controller。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (3, 'curd', 'service', '/templates/end/service.java.vm', '%s/src/main/java/%s/service/%sService.java', b'1', b'0', '参数描述：数据库名或者sql文件名,基础包名，表名转换的类名,用于生成Service。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (4, 'curd', 'impl', '/templates/end/impl.java.vm', '%s/src/main/java/%s/service/impl/%sServiceImpl.java', b'1', b'0', '参数描述：数据库名或者sql文件名,基础包名，表名转换的类名,用于生成Impl。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (5, 'curd', 'mapper', '/templates/end/mapper.java.vm', '%s/src/main/java/%s/mapper/%sMapper.java', b'1', b'0', '参数描述：数据库名或者sql文件名,基础包名，表名转换的类名,用于生成Mapper。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (6, 'curd', 'mapper.xml', '/templates/end/mapper.xml.vm', '%s/src/main/resources/mapper/%sMapper.xml', b'0', b'0', '参数描述：数据库名或者sql文件名,基础包名，表名转换的类名,用于生成Mpper.xml。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (7, 'base', NULL, '/templates/base/MainApp.java.vm', '%s/src/main/java/%s/MainApp.java', b'1', b'0', '参数描述：数据库名或者sql文件名,基础包名，用于生成主启动类。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (8, 'base', NULL, '/templates/base/application.yml.vm', '%s/src/main/resources/application.yml', b'1', b'0', '参数描述：数据库名或者sql文件名,用于生成配置文件。', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (9, 'base', NULL, '/templates/base/pom.xml.vm', '%s/pom.xml', b'0', b'0', '参数描述：数据库名或者sql文件名,用于生成pom.xml,', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (10, 'log', NULL, '/templates/aspect/Log.java.vm', '%s/src/main/java/%s/aspect/Log.java', b'1', b'0', '参数描述：数据库名或者sql文件名,用于生成pom.xml,', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (11, 'log', NULL, '/templates/aspect/LogAspect.java.vm', '%s/src/main/java/%s/aspect/LogAspect.java', b'1', b'0', '参数描述：数据库名或者sql文件名,用于生成pom.xml,', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (12, 'log', NULL, '/templates/enums/BussinessEnum.java', '%s/src/main/java/%s/enums/BussinessEnum.java', b'1', b'0', '参数描述：数据库名或者sql文件名,用于生成pom.xml,', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (13, 'log', NULL, '/templates/enums/OperationEnum.java', '%s/src/main/java/%s/enums/OperationEnum.java', b'1', b'0', '参数描述：数据库名或者sql文件名,用于生成pom.xml,', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (14, 'log', NULL, '/templates/aspect/SysOperationLog.java.vm', '%s/src/main/java/%s/entity/SysOperationLog.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (15, 'utils', NULL, '/templates/utils/EscapeUtil.java.vm', '%s/src/main/java/%s/utils/EscapeUtil.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (16, 'utils', NULL, '/templates/utils/IpUtils.java.vm', '%s/src/main/java/%s/utils/IpUtils.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (17, 'utils', NULL, '/templates/utils/ServletUtils.java.vm', '%s/src/main/java/%s/utils/ServletUtils.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (18, 'utils', NULL, '/templates/utils/SecurityUtils.java.vm', '%s/src/main/java/%s/utils/SecurityUtils.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (19, 'config', NULL, '/templates/config/GlobalExceptionHandler.java.vm', '%s/src/main/java/%s/config/GlobalExceptionHandler.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (20, 'config', NULL, '/templates/config/KnifeConfig.java.vm', '%s/src/main/java/%s/config/KnifeConfig.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (21, 'config', NULL, '/templates/config/RedisConfig.java.vm', '%s/src/main/java/%s/config/RedisConfig.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (22, 'common', NULL, '/templates/common/BaseEntity.java.vm', '%s/src/main/java/%s/common/BaseEntity.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (23, 'common', NULL, '/templates/common/PageParam.java.vm', '%s/src/main/java/%s/common/PageParam.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (24, 'common', NULL, '/templates/common/Result.java.vm', '%s/src/main/java/%s/common/Result.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (25, 'common', NULL, '/templates/common/ResultFactory.java.vm', '%s/src/main/java/%s/common/ResultFactory.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (26, 'common', NULL, '/templates/common/Consts.java.vm', '%s/src/main/java/%s/common/Consts.java', b'1', b'0', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (27, 'vue', NULL, '/templates/front/list.vue.vm', '%s/src/views/%s/index.vue', b'0', b'1', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (28, 'vue', NULL, '/templates/front/form.vue.vm', '%s/src/views/%s/form.vue', b'0', b'1', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (29, 'vue', NULL, '/templates/front/router.js.vm', '%s/src/router/%s/index.js', b'0', b'1', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (30, 'vue', NULL, '/templates/front/api.js.vm', '%s/src/api/%s.js', b'0', b'1', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (31, 'components', NULL, '/templates/front/Pagination.vue.vm', '%s/src/components/Pagination/index.vue', b'0', b'1', '', NULL, NULL, NULL, NULL, b'0');
INSERT INTO `out_path_config` VALUES (32, 'components', NULL, '/templates/front/scroll-to.js', '%s/src/utils/scroll-to.js', b'0', b'1', '', NULL, NULL, NULL, NULL, b'0');

SET FOREIGN_KEY_CHECKS = 1;
